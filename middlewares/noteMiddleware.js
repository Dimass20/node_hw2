const mongoose = require('mongoose');

exports.isValidIdOfNote = (req, res, next) => {
  const isValidId = mongoose.Types.ObjectId.isValid(req.params.idNote);

  if (!isValidId) {
    return res.status(400).json({'message': 'Id of note isn\'t valid'});
  }

  next();
};
