require('dotenv').config();
const jwt = require('jsonwebtoken');
const userModel = require('../models/userModel');

exports.validUserData = function(req, res, next) {
  const {password, username} = req.body;

  if (password == null || username == null) {
    return res.status(400).json({
      'message': 'password and username are required',
    });
  }

  next();
};


exports.validToken = (req, res, next) => {
  const header = req.headers['authorization'];

  if (!header) {
    return res.status(400).json({message: `No JWT token found!`});
  }

  try {
    const [tokenType, token] = header.split(' ');
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    req.idUser = decoded.id;
    next();
  } catch (err) {
    return res.status(400).json({message: `No JWT token valid!`});
  }
};


exports.userExists = async (req, res, next) => {
  try {
    const user = await userModel.findById(req.idUser).exec();

    if (user == null) {
      return res.status(400).json({message: `The user does not exist`});
    }
  } catch (err) {
    return res.status(500).json(err);
  }
  next();
};
