const express = require('express');
const notesRouter = new express.Router();
const {validToken, userExists} = require('./../middlewares/userData');
const {isValidIdOfNote} = require('./../middlewares/noteMiddleware');
const {
  createNote,
  getNoteById,
  updateNote,
  completedNote,
  deleteNote,
  getAllNotes,
} = require('./../controllers/notesController');


notesRouter.get('/', validToken, userExists, getAllNotes);

notesRouter
    .get('/:idNote', validToken, userExists, isValidIdOfNote, getNoteById);

notesRouter.post('/', validToken, userExists, createNote);

notesRouter
    .put('/:idNote', validToken, userExists, isValidIdOfNote, updateNote);

notesRouter
    .patch('/:idNote', validToken, userExists, isValidIdOfNote, completedNote);

notesRouter
    .delete('/:idNote', validToken, userExists, isValidIdOfNote, deleteNote);


module.exports = notesRouter;
