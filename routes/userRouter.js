const express = require('express');
const userRouter = new express.Router();
const {validToken, userExists} = require('./../middlewares/userData');
const {
  getInfoUser,
  updatePsswrdUser,
  deleteUser,
} = require('../controllers/userController');


userRouter.get('/me', validToken, userExists, getInfoUser);
userRouter.delete('/me', validToken, userExists, deleteUser);
userRouter.patch('/me', validToken, userExists, updatePsswrdUser);


module.exports = userRouter;
