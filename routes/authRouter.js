const express = require('express');
const authRouter = new express.Router();
const {validUserData} = require('./../middlewares/userData');
const {
  registerController,
  loginController,
} = require('../controllers/authController');

authRouter.post('/register', validUserData, registerController);
authRouter.post('/login', validUserData, loginController);

module.exports = authRouter;
