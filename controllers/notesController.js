const noteModel = require('./../models/noteModel');


exports.createNote = async (req, res) => {
  const {text} = req.body;

  if (text == null) {
    return res.status(400).json({
      'message': 'text is required',
    });
  }

  try {
    const note = noteModel({text: text, userId: req.idUser});
    await note.save();
    return res.json({'message': 'Success'});
  } catch (err) {
    return res.status(500).json(err);
  }
};


exports.getNoteById = async (req, res) => {
  const {idNote} = req.params;

  try {
    const note = await noteModel.findById(idNote).exec();

    if (note == null || note.userId != req.idUser) {
      return res.status(400).json({'message': 'not found'});
    }

    return res.json({
      'note': {
        '_id': note._id,
        'userId': note.userId,
        'completed': note.completed,
        'text': note.text,
        'createdDate': note.createdAt,
      },
    });
  } catch (err) {
    return res.status(500).json(err);
  }
};

exports.updateNote = async (req, res) => {
  const {idNote} = req.params;
  const {text} = req.body;

  try {
    if (text == null) {
      return res.status(400).json({
        message: 'text is required',
      });
    }

    const note = await noteModel.findById(idNote).exec();
    if (note == null || note.userId != req.idUser) {
      return res.status(400).json({'message': 'not found'});
    }

    await noteModel.findOneAndUpdate({_id: idNote}, {text: text});
    return res.json({'message': 'Success'});
  } catch (err) {
    return res.status(500).json(err);
  }
};


exports.completedNote = async (req, res) => {
  const {idNote} = req.params;

  try {
    const note = await noteModel.findById(idNote).exec();
    if (note == null || note.userId != req.idUser) {
      return res.status(400).json({'message': 'not found'});
    }

    await noteModel.findOneAndUpdate(
        {_id: idNote}, {completed: !note.completed},
    );
    return res.json({'message': 'Success'});
  } catch (err) {
    return res.status(500).json(err);
  }
};


exports.deleteNote = async (req, res) => {
  const {idNote} = req.params;

  try {
    const note = await noteModel.findById(idNote).exec();
    if (note == null || note.userId != req.idUser) {
      return res.status(400).json({'message': 'not found'});
    }

    await noteModel.findByIdAndDelete(idNote);
    return res.json({'message': 'Success'});
  } catch (err) {
    return res.status(500).json(err);
  }
};


exports.getAllNotes = async (req, res) => {
  try {
    const notes = await noteModel.find({userId: req.idUser});

    if (notes == null || notes.length == 0) {
      return res.status(200).json({'notes': []});
    }

    const arrNotes = [];

    notes.forEach((note) => {
      arrNotes.push({
        _id: note._id,
        userId: note.userId,
        completed: note.completed,
        text: note.text,
        createdDate: note.createdAt,
      });
    });

    return res.json({'notes': [...arrNotes]});
  } catch (err) {
    return res.status(500).json(err);
  }
};

