const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const noteScheme = new Schema(
    {
      text: {type: String, required: true},
      completed: {type: Boolean, default: false, required: true},
      userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users',
      },
    },
    {
      timestamps: true,
    },
);


module.exports = mongoose.model('notes', noteScheme);

