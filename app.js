require('dotenv').config();
require('./database');
const express = require('express');
const morgan = require('morgan');
const PORT = process.env.PORT || 8080;
const app = express();
const userRouter = require('./routes/userRouter');
const notesRouter = require('./routes/notesRouter');
const authRouter = require('./routes/authRouter.js');


app.use(express.json());
app.use(morgan('tiny'));


app.use('/api/users', userRouter);
app.use('/api/notes', notesRouter);
app.use('/api/auth', authRouter);


app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});
