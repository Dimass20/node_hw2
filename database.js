const mongoose = require('mongoose');
const dbURI = 'mongodb+srv://dmitry:qwerty1@cluster0.idjsy.mongodb.net/epam?retryWrites=true&w=majority';

(async () => {
  try {
    mongoose.Promise = global.Promise;
    mongoose.set('useFindAndModify', false);
    await mongoose.connect(dbURI, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useCreateIndex: true,
      useNewUrlParser: true,
    });
  } catch (err) {
    console.log('error: ' + err);
  }
})();
